<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
     <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">  
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet">  
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>  
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>  
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js"></script>

  <script>
  $(function() {
    $( "#fromdatepicker").datepicker({
                    format: "yyyy-mm-dd", 
					autoclose: true 
                });
	$( "#todatepicker").datepicker({
                    format: "yyyy-mm-dd", 
					autoclose: true 
                });
  });
  </script>
  </head>
  <body>
    <div class="container">
      <h2 align="center">Create A Coupon</h2><br  />
      @if ($errors->any())
      <div class="alert alert-danger">
          <ul>
          
              @foreach ($errors->all() as $error)
                  <li>{{ $error }}</li>
              @endforeach
          </ul>
      </div><br />
      @endif
      @if (\Session::has('success'))
      <div class="alert alert-success">
          <p>{{ \Session::get('success') }}</p>
      </div><br />
      @endif
      <form method="post" action="{{url('coupons')}}">
        {{csrf_field()}}
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <label for="name">Name:</label>
            <input type="text" class="form-control" name="name">
          </div>
        </div>
        <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Amount:</label>
              <input type="text" class="form-control" name="amount">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Max Amount:</label>
              <input type="text" class="form-control" name="max_redeem">
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Max Amount Per User:</label>
              <input type="text" class="form-control" name="max_redeem_per_user">
            </div>
          </div>
          
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">Description:</label>
			  <textarea name="description" id="description" class="form-control" ></textarea>
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">From Date:</label>
			 <input type="text" id="fromdatepicker" name="valid_from_date" class="form-control" value="" /> 
            </div>
          </div>
          <div class="row">
          <div class="col-md-4"></div>
            <div class="form-group col-md-4">
              <label for="price">To Date:</label>
			 <input type="text" id="todatepicker" name="valid_to_date" class="form-control" value="" /> 
            </div>
          </div>
        </div>
          
        </div>
        <div class="row">
          <div class="col-md-4"></div>
          <div class="form-group col-md-4">
            <button type="submit" class="btn btn-success" style="margin-left:38px">Add Coupon</button>
          </div>
        </div>
      </form>
    </div>
  </body>
</html>