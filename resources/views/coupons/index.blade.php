
<!doctype html>
<html>
<head>
   <link rel="stylesheet" href="{{asset('css/app.css')}}">
</head>
<body>
<div class="container">
	
    <?php /*?> @foreach($staticpages as $staticpage)
    <tr><td>{{$staticpage['page']}}</td></tr>
    @endforeach
    <?php */?>
    
       <h2 align="center"><span>Manage Coupons</span></h2>
       <a href="{{action('CouponsController@create')}}" style="float:right;"><h3>Add Coupon</h3></a>
        @if (\Session::has('success'))
        
        <div class="alert alert-success" style="clear:both;">
        <p>{{ \Session::get('success') }}</p>
      </div>
     @endif
    <table class="table table-striped"  border="1"> 
    <thead>
      <tr>
        <th>ID</th>
        <th>Name</th>
        <th>Amount</th>
        <th colspan="2">Action</th>
      </tr>
    </thead>
    <tbody>
    
    @if(count($coupons))
      @foreach($coupons as $coupon)
      <tr>
        <td>{{$coupon['id']}}</td>
        <td>{{$coupon['name']}}</td>
        <td>{{$coupon['amount']}}</td>
        <td><a href="{{action('CouponsController@edit', $coupon['id'])}}" class="btn btn-warning">Edit</a></td>
        <td>
          <form action="{{action('CouponsController@destroy', $coupon['id'])}}" method="post">
            {{csrf_field()}}
            <input name="_method" type="hidden" value="DELETE">
            <button class="btn btn-danger" type="submit">Delete</button>
          </form>
        </td>
      </tr>
      @endforeach
      @else
      	<tr><td colspan="5" align="center">Coupon Not Available</td></tr>
      @endif
      
      
    </tbody>
  </table>
  
    
  </div>
  </body>
</html>