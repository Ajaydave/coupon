<?php

namespace App\Http\Controllers;
use App\Coupon;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;

class CouponsController extends Controller
{
   
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	     $coupons = Coupon::all()->toArray();
		 return view('coupons.index', compact('coupons'));
    }
	public function getContent()
	{
		$getarray=array('test');
		return $getarray;
	}
	/**
     * Create a new collection using the collect helper method.
     */
    public function helperCollection()
    {
        $newCollection = collect([1, 2, 3, 4, 5]);
        dd($newCollection);
    }
	
	/**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
   
	    return view('coupons.create');
    }
	/**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

		$max_redeem = $request->get('max_redeem');
        $coupon = $this->validate(request(), [
          'name' => 'required',
		  'amount' => 'required|numeric',
		  'max_redeem' => 'required|numeric',
		  'max_redeem_per_user' => 'required|numeric|required_with:max_redeem|integer|max:'.($max_redeem),
		  'description' => 'required',
		  'valid_from_date' => 'required',
		  'valid_to_date' => 'required|date|date_format:Y-m-d|after:valid_from_date'
        ]);
        
    	    Coupon::create($coupon);
	        return redirect('coupons')->with('success','Coupon has been added');
    }
	 /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $coupon = Coupon::find($id);
        return view('coupons.edit',compact('coupon','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $coupon = Coupon::find($id);
		$max_redeem = $request->get('max_redeem');

        $this->validate(request(), [
          'name' => 'required',
		  'amount' => 'required|numeric',
		  'max_redeem' => 'required|numeric',
		  'max_redeem_per_user' => 'required|numeric|required_with:max_redeem|integer|max:'.($max_redeem),
		  'description' => 'required',
		  'valid_from_date' => 'required',
		  'valid_to_date' => 'required|date|date_format:Y-m-d|after:valid_from_date'
        ]);
        $coupon->name = $request->get('name');
        $coupon->amount = $request->get('amount');
		$coupon->description = $request->get('description');
		$coupon->max_redeem = $request->get('max_redeem');
		$coupon->max_redeem_per_user = $request->get('max_redeem_per_user');
		$coupon->valid_to_date = $request->get('valid_to_date');
		$coupon->valid_from_date = $request->get('valid_from_date');

        $coupon->save();
        return redirect('coupons')->with('success','Coupon has been updated');
    }
   /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $coupon = Coupon::find($id);
        $coupon->delete();
        return redirect('coupons')->with('success','Coupon has been  deleted');
    }
}
